package org.example.project.controllers;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.example.project.BD;
import org.example.project.BudgetApplication;

import java.io.IOException;
import java.sql.*;
import java.time.LocalDate;

public  class BalanceController {


    public static double getTotalIncome() {

        double totalIncome = 0;
        try {
            Connection connection = DriverManager.getConnection(BD.url, BD.user, BD.password);
            Statement statement = connection.createStatement();
            String incomesQuery = "SELECT SUM(amount) FROM Incomes";
            ResultSet resultSet = statement.executeQuery(incomesQuery);
            if (resultSet.next()) {
                totalIncome = resultSet.getDouble(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return totalIncome;
    }

    public static double getTotalExpense() {
        double totalExpense = 0;
        try {
            Connection connection = DriverManager.getConnection(BD.url, BD.user, BD.password);
            Statement statement = connection.createStatement();
            String incomesQuery = "SELECT SUM(amount) FROM Expenses";
            ResultSet resultSet = statement.executeQuery(incomesQuery);
            if (resultSet.next()) {
                totalExpense = resultSet.getDouble(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return totalExpense;
    }
    public static double getTotalIncomeForMonth() {
        double totalIncome = 0;
        try (Connection connection = DriverManager.getConnection(BD.url, BD.user, BD.password)) {
            String sql = "SELECT SUM(amount) FROM Incomes WHERE EXTRACT(YEAR FROM time_and_date) = ? AND EXTRACT(MONTH FROM time_and_date) = ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, LocalDate.now().getYear());
            statement.setInt(2, LocalDate.now().getMonthValue());
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                totalIncome = resultSet.getDouble(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return totalIncome;

    }

    public static double getTotalExpenseForMonth() {
        double totalExpense = 0;
        try (Connection connection = DriverManager.getConnection(BD.url, BD.user, BD.password)) {
            String sql = "SELECT SUM(amount) FROM Expenses WHERE EXTRACT(YEAR FROM time_and_date) = ? AND EXTRACT(MONTH FROM time_and_date) = ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, LocalDate.now().getYear());
            statement.setInt(2, LocalDate.now().getMonthValue());
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                totalExpense = resultSet.getDouble(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return totalExpense;
    }

    public static double getTotalIncomeForYear() {
        double totalIncome = 0;
        try (Connection connection = DriverManager.getConnection(BD.url, BD.user, BD.password)) {
            String sql = "SELECT SUM(amount) FROM Incomes WHERE EXTRACT(YEAR FROM time_and_date) = ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, LocalDate.now().getYear());
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                totalIncome = resultSet.getDouble(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return totalIncome;
    }

    public static double getTotalExpenseForYear() {
        double totalExpense = 0;
        try (Connection connection = DriverManager.getConnection(BD.url, BD.user, BD.password)) {
            String sql = "SELECT SUM(amount) FROM Expenses WHERE EXTRACT(YEAR FROM time_and_date) = ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, LocalDate.now().getYear());
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                totalExpense = resultSet.getDouble(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return totalExpense;
    }

    public static void choosePeriod() throws IOException {
        FXMLLoader fxmlLoader = new
                FXMLLoader(BudgetApplication.class.getResource("choose_period.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 400, 300);
        DatePicker startDatePicker = new DatePicker();
        DatePicker endDatePicker = new DatePicker();
        Button applyButton = new Button("Застосувати");
        Stage primaryStage = new Stage();
        primaryStage.setTitle("Обрати період");
        primaryStage.setScene(scene);
        primaryStage.initModality(Modality.APPLICATION_MODAL);
        primaryStage.showAndWait();

    }

  /*  private  void getBalance(DatePicker start, DatePicker end) {
        BigDecimal totalIncome = GetIncomesForPeriod(start, end);
        BigDecimal totalExpense = GetExpensesForPeriod(start, end);
        totalIncomeLabel.setText(totalIncome.toString());
        totalExpenseLabel.setText(totalExpense.toString());
    }

/*
   public static double GetIncomesForPeriod(DatePicker startDate, DatePicker endDate) {
        LocalDate startDateValue = startDate.getValue();
        LocalDate endDateValue = endDate.getValue();
      double totalIncome = 0;
        try (Connection connection = DriverManager.getConnection(BD.url, BD.user, BD.password)) {
            String sql = "SELECT SUM(amount) FROM Incomes WHERE time_and_date BETWEEN ? AND ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setObject(1, startDateValue);
            statement.setObject(2, endDateValue);
            ResultSet resultSet = statement.executeQuery();


            if (resultSet.next()) {
                totalIncome = resultSet.getDouble(1);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return totalIncome;


    }
    @FXML
        public static double GetExpensesForPeriod(DatePicker startDate, DatePicker endDate) {
        LocalDate startDateValue = startDate.getValue();
        LocalDate endDateValue = endDate.getValue();
        double totalExpenses =0;
        try (Connection connection = DriverManager.getConnection(BD.url, BD.user, BD.password)) {
            String sql = "SELECT SUM(amount) FROM Expenses WHERE time_and_date BETWEEN ? AND ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setObject(1, startDateValue);
            statement.setObject(2, endDateValue);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                totalExpenses = resultSet.getDouble(1);
            }


        } catch (SQLException e) {
            e.printStackTrace();
        }
        return totalExpenses;
    }*/


}
