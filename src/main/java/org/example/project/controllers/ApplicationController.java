package org.example.project.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;

import java.io.IOException;

public class ApplicationController {

    @FXML
    private DatePicker startDate;
    @FXML
    private DatePicker endDate;
    @FXML
    private     Label totalIncomeLabel;

    @FXML
    private      Label totalExpenseLabel;

    @FXML
    private void handleDisplay(double totalIncome, double totalExpense) {
        totalIncomeLabel.setText(String.valueOf(totalIncome));
        totalExpenseLabel.setText(String.valueOf(totalExpense));
    }
    public void initialize() {
        double totalIncome = BalanceController.getTotalIncome();
        double totalExpense = BalanceController.getTotalExpense();
        this.handleDisplay(totalIncome, totalExpense);

    }
    public void handleMonthButtonClick() {
        double totalIncome = BalanceController.getTotalIncomeForMonth();
        double totalExpense = BalanceController.getTotalExpenseForMonth();
        this.handleDisplay(totalIncome, totalExpense);
    }
    public void handleYearButtonClick() {
        double totalIncome = BalanceController.getTotalIncomeForYear();
        double totalExpense = BalanceController.getTotalExpenseForYear();
        this.handleDisplay(totalIncome, totalExpense);
    }

   public void handlePeriodButtonClick() throws IOException {
       BalanceController.choosePeriod();
//           Stage stage = new Stage();
//           DatePicker startDatePicker = new DatePicker();
//           DatePicker endDatePicker = new DatePicker();
//           Button applyButton = new Button("Застосувати");
//
//           applyButton.setOnAction(e -> {
//               double totalIncome = BalanceController.GetIncomesForPeriod(startDatePicker, endDatePicker);
//               double totalExpense = BalanceController.GetExpensesForPeriod(startDatePicker, endDatePicker);
//               this.handleDisplay(totalIncome, totalExpense);
//               stage.close();
//           });
//
//           VBox vbox = new VBox(50);
//           Scene scene = new Scene(vbox, 300, 300);
//           vbox.getChildren().addAll(startDatePicker, endDatePicker, applyButton);
//
//           stage.setScene(scene);
//
//           stage.show();


    }


    public void addExpensesButtonClick() throws IOException {
        AddExpensesController.addExpense();
    }

    public void addIncomesButtonClick() throws IOException {
        AddIncomesController.addIncome();
    }
    public void addCategoriesButtonClick() throws IOException {
        AddCategoriesController.addCategory();
    }
    public void aboutProgramButtonClick() throws IOException {
       InformationController.aboutProgram();
    }
    public void aboutDevelopersButtonClick() throws IOException {
        InformationController.aboutDevelopers();
    }
    public  void editCategoriesButtonClick() throws Exception {
        EditCategoriesController.editCategory();
    }
}
