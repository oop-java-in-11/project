package org.example.project.controllers;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.example.project.BudgetApplication;

import java.io.IOException;

public class AddCategoriesController extends Application {

    public static void addCategory() throws IOException {

        FXMLLoader fxmlLoader = new
                FXMLLoader(BudgetApplication.class.getResource("add-category.fxml"));
        ScrollPane scrollPane = new ScrollPane(fxmlLoader.load());
        Scene scene = new Scene(scrollPane, 300, 300);
        Stage primaryStage = new Stage();

        primaryStage.setTitle("Додати категорію");
        primaryStage.setScene(scene);

        primaryStage.initModality(Modality.APPLICATION_MODAL);
        primaryStage.showAndWait();
    }

    public void start(Stage stage) {
    }


}
