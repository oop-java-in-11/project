module org.example.project {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;
    requires java.persistence;

    opens org.example.project to javafx.fxml;
    exports org.example.project;
    exports org.example.project.entities;
    opens org.example.project.entities to javafx.fxml;
    exports org.example.project.controllers;
    opens org.example.project.controllers to javafx.fxml;
}