package org.example.project.controllers;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.example.project.BD;
import org.example.project.BudgetApplication;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class EditCategoriesController extends Application {


    private static Stage editCategoryStage;
/*
    public static void editCategory() throws IOException, SQLException {

        categoryListView.getItems();

        ScrollPane scrollPane = new ScrollPane(fxmlLoader.load());
        Scene scene = new Scene(scrollPane, 300, 300);
        Stage primaryStage = new Stage();

        primaryStage.setTitle("редагувати категорію");
        primaryStage.setScene(scene);

        Connection connection = DriverManager.getConnection(BD.url, BD.user, BD.password);
        Statement statement = connection.createStatement();


        String categoriesQuery = "SELECT * FROM Categories";
        ResultSet resultSetCategories = statement.executeQuery(categoriesQuery);

        List<String> categories = new ArrayList<>();
        while (resultSetCategories.next()) {
            String categoryName = resultSetCategories.getString("name");
            categories.add(categoryName);
        }

        categoryListView.getItems().addAll(categories);
        System.out.println(categoryListView.getItems());
        connection.close();



        primaryStage.initModality(Modality.APPLICATION_MODAL);
        primaryStage.showAndWait();
    }
*/



        public static void editCategory() throws Exception {
            FXMLLoader fxmlLoader = new
                    FXMLLoader(BudgetApplication.class.getResource("edit_categories.fxml"));
            Stage primaryStage = new Stage();

            ListView<String> categoryListView = new ListView<>();
            ObservableList<String> categories = FXCollections.observableArrayList(getCategoriesFromDB());
            categoryListView.setItems(categories);

            StackPane root = new StackPane();
            root.getChildren().add(categoryListView);

            Scene scene = new Scene(root, 300, 250);

            primaryStage.setTitle("Category List View");
            primaryStage.setScene(scene);
            primaryStage.show();
            categoryListView.setOnMouseClicked(event -> {
                String selectedCategory = categoryListView.getSelectionModel().getSelectedItem();
                if (selectedCategory != null) {
                    openEditCategoryWindow(selectedCategory, categoryListView);
                }
            });
        }

    private static void openEditCategoryWindow(String category, ListView<String> categoryList) {
        if (editCategoryStage == null) {
            editCategoryStage = new Stage();
            editCategoryStage.setTitle("Edit Category");
            VBox editCategoryLayout = new VBox(10);


            TextField categoryField = new TextField(category);
            Button saveButton = new Button("Save");

            saveButton.setOnAction(event -> {
                String updatedCategory = categoryField.getText();
               
                updateCategoryInDB(category, updatedCategory);

                categoryList.getItems().set(categoryList.getSelectionModel().getSelectedIndex(), updatedCategory);


                editCategoryStage.close();
            });

            editCategoryLayout.getChildren().addAll(new Label("Edit Category"), categoryField, saveButton);
            Scene scene = new Scene(editCategoryLayout, 300, 150);
            editCategoryStage.setScene(scene);
        }
        editCategoryStage.show();
    }

    private static void updateCategoryInDB(String oldCategory, String newCategory) {
        try (Connection conn = DriverManager.getConnection(BD.url, BD.user, BD.password)) {
            String query = "UPDATE categories SET name = ? WHERE name = ?";
            try (PreparedStatement stmt = conn.prepareStatement(query)) {
                stmt.setString(1, newCategory);
                stmt.setString(2, oldCategory);
                stmt.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    private static List<String> getCategoriesFromDB() {
            List<String> categories = new ArrayList<>();

            try (Connection conn =DriverManager.getConnection(BD.url, BD.user, BD.password)) {
                String query = "SELECT name FROM categories";
                try (Statement stmt = conn.createStatement()) {
                    try (ResultSet rs = stmt.executeQuery(query)) {
                        while (rs.next()) {
                            categories.add(rs.getString("name"));
                        }
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

            return categories;
        }


    public void start(Stage stage) throws Exception {

    }

}
