package org.example.project.controllers;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.example.project.BudgetApplication;

import java.io.IOException;

public class InformationController extends Application {
        public static void aboutDevelopers() throws IOException {

            FXMLLoader fxmlLoader = new
                    FXMLLoader(BudgetApplication.class.getResource("about-developers.fxml"));
            Scene scene = new Scene(fxmlLoader.load(), 600, 400);
            Stage primaryStage = new Stage();
            primaryStage.setTitle("Про розробників");
            primaryStage.setScene(scene);
            primaryStage.initModality(Modality.APPLICATION_MODAL);
            primaryStage.showAndWait();


        }
        public static void aboutProgram() throws IOException {
            FXMLLoader fxmlLoader = new
                    FXMLLoader(BudgetApplication.class.getResource("about-program.fxml"));
            Scene scene = new Scene(fxmlLoader.load(), 600, 400);
            Stage primaryStage = new Stage();
            primaryStage.setTitle("Про програму");
            primaryStage.setScene(scene);
            primaryStage.initModality(Modality.APPLICATION_MODAL);
            primaryStage.showAndWait();

        }
        public void start(Stage stage) {
        }

}
