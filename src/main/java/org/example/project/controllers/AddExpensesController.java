package org.example.project.controllers;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ScrollPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.example.project.BudgetApplication;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class AddExpensesController extends Application implements Initializable {

    @FXML
    private ComboBox<String> categoryComboBox;
    public static void addExpense() throws IOException {

        FXMLLoader fxmlLoader = new
                FXMLLoader(BudgetApplication.class.getResource("add-expenses.fxml"));
        ScrollPane scrollPane = new ScrollPane(fxmlLoader.load());
        Scene scene = new Scene(scrollPane, 300, 300);
        Stage primaryStage = new Stage();

        primaryStage.setTitle("Додати витрати");
        primaryStage.setScene(scene);

        primaryStage.initModality(Modality.APPLICATION_MODAL);
        primaryStage.showAndWait();
    }

    public void start(Stage stage) {
    }
    @Override

    public void initialize(URL location, ResourceBundle resources) {
        categoryComboBox.setVisibleRowCount(5);
        categoryComboBox.getItems().addAll("Елемент 1", "Елемент 2", "Елемент 3");
    }

}
